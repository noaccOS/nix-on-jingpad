# Nix on Jingpad

Nix installer for UBPorts on the jingpad, taking into consideration the immutable filesystem and lack of xz support

## Usage

Download the install script from this repo or use `wget -qO - https://gitlab.com/noaccOS/nix-on-jingpad/-/raw/main/install | bash [-s param]`

The only optional parameter is the installation directory for nix, which defaults to `~/.nix`
