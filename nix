#!/bin/sh

# Nix installation script

# This script installs the Nix package manager on your system by
# downloading a binary distribution and running its installer script
# (which in turn creates and populates /nix).

{ # Prevent execution if this script was only partially downloaded
oops() {
    echo "$0:" "$@" >&2
    exit 1
}

tmpDir="$(mktemp -d -t nix-binary-tarball-unpack.XXXXXXXXXX || \
          oops "Can't create temporary directory for downloading the Nix binary tarball")"
cleanup() {
    rm -rf "$tmpDir"
}
trap cleanup EXIT INT QUIT TERM

version="2.13.2"
url="https://gitlab.com/noaccOS/nix-on-jingpad/-/raw/$version/nix-release.tar.gz"
hash="bb9bfd751c68a9f003098e90518cec6c7eff3eb8e05e1493347de96a2467e5bb"
tarball="$tmpDir/nix-release.tar.gz"

umask 0022

require_util() {
    command -v "$1" > /dev/null 2>&1 ||
        oops "you do not have '$1' installed, which I need to $2"
}

require_util tar "unpack the binary tarball"

echo "downloading Nix $version binary tarball from '$url' to '$tmpDir'..."
wget "$url" -O "$tarball" || oops "failed to download '$url'"

hash2="$(sha256sum -b "$tarball" | cut -c1-64)"
if [ "$hash" != "$hash2" ]; then
    oops "SHA-256 hash mismatch in '$url'; expected $hash, got $hash2"
fi

unpack="$tmpDir/unpack"
mkdir -p "$unpack"
tar -xzf "$tarball" -C "$unpack" || oops "failed to unpack '$url'"

script=$(echo "$unpack"/*/install)

[ -e "$script" ] || oops "installation script is missing from the binary tarball!"
export INVOKED_FROM_INSTALL_IN=1
"$script" "$@"

} # End of wrapping
